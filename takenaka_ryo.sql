-- MySQL dump 10.13  Distrib 5.6.11, for Win64 (x86_64)
--
-- Host: localhost    Database: takenaka_ryo
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'東京本社','2018-06-22 03:08:44','2018-06-22 03:08:44'),(2,'新宿支店','2018-06-22 04:21:05','2018-06-22 04:21:05'),(3,'横浜支店','2018-06-22 04:21:49','2018-06-22 04:21:49'),(4,'名古屋支店','2018-06-22 04:22:27','2018-06-22 04:22:27');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contribution_id` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (309,2,20,'hoge','2018-06-18 06:31:53','2018-06-18 06:31:53'),(310,2,20,'888','2018-06-18 06:32:34','2018-06-18 06:32:34'),(312,2,19,'hoge2','2018-06-18 07:22:51','2018-06-18 07:22:51'),(313,2,18,'hoge','2018-06-18 07:54:35','2018-06-18 07:54:35'),(315,6,22,'1111111','2018-06-21 05:45:32','2018-06-21 05:45:32'),(316,8,22,'hoge','2018-06-22 07:20:42','2018-06-22 07:20:42'),(322,2,23,'aa','2018-06-25 06:42:24','2018-06-25 06:42:24'),(323,2,23,'aaaa','2018-06-25 06:42:53','2018-06-25 06:42:53'),(327,2,30,'t\r\ne\r\ns\r\nt','2018-06-26 05:53:05','2018-06-26 05:53:05'),(328,1,31,'t\r\ne\r\ns\r\nt','2018-06-27 02:38:38','2018-06-27 02:38:38');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contributions`
--

DROP TABLE IF EXISTS `contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `category` varchar(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contributions`
--

LOCK TABLES `contributions` WRITE;
/*!40000 ALTER TABLE `contributions` DISABLE KEYS */;
INSERT INTO `contributions` VALUES (11,0,'111111','111111','111111','2018-06-15 04:42:04','2018-06-15 04:42:04'),(12,0,'11','1111','111','2018-06-15 04:44:10','2018-06-15 04:44:10'),(13,0,'aaaa','aaaa','aaaa','2018-06-15 04:46:10','2018-06-15 04:46:10'),(14,2,'aaaa','aaaa','aaaaa','2018-06-15 04:49:55','2018-06-15 04:49:55'),(15,2,'aaaa','aaaaa','aaaa','2018-06-15 04:50:49','2018-06-15 04:50:49'),(16,2,'11','1111','11111','2018-06-15 06:26:35','2018-06-15 06:26:35'),(17,2,'11','1111','11111','2018-06-15 06:28:09','2018-06-15 06:28:09'),(20,2,'test','testtest','test','2018-06-15 07:33:04','2018-06-15 07:33:04'),(21,1,'12345','12345','12345','2018-06-18 08:18:08','2018-06-18 08:18:08'),(22,6,'1111111','1111111','1111111','2018-06-21 05:44:39','2018-06-21 05:44:39'),(23,1,'test','honbun4\r\nkigyou','cate','2018-06-25 01:14:56','2018-06-25 01:14:56'),(26,1,'aa','aa\r\naa','aa','2018-06-26 04:45:42','2018-06-26 04:45:42'),(28,1,'aaa','aaa\r\naaaa','aaaa','2018-06-26 05:43:44','2018-06-26 05:43:44'),(29,1,'aaa','111\r\n222\r\n333','pppp','2018-06-26 05:46:42','2018-06-26 05:46:42'),(30,2,'1111','t\r\ne\r\ns\r\nt','test','2018-06-26 05:47:31','2018-06-26 05:47:31'),(31,1,'test','t\r\ne\r\ns\r\nt','testtest','2018-06-27 02:36:26','2018-06-27 02:36:26');
/*!40000 ALTER TABLE `contributions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'総務部','2018-06-22 04:27:02','2018-06-22 04:27:02'),(2,'情報管理部','2018-06-22 04:27:57','2018-06-22 04:27:57'),(3,'支店長','2018-06-22 04:32:52','2018-06-22 04:32:52'),(4,'社員','2018-06-22 05:02:14','2018-06-22 05:02:14');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) NOT NULL,
  `name` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test123','社員1','7NcYcNGWMxapfjrDQIyYNa2M8PPBvHA1J8MCZVNPda4',1,1,0,'2018-06-12 06:25:25','2018-06-27 04:55:20'),(2,'test456','社員2','UOSe_eTjfYS5exSsT30DupVvFBP3NnCQuuq6xnv2lwQ',1,2,0,'2018-06-13 06:20:15','2018-06-28 00:45:43'),(5,'test789','社員3','LQ6t-oWk-yLPzLP7OnMaOnRST3F39CWdkgpbvP_vwEA',3,3,0,'2018-06-19 02:16:10','2018-06-22 08:01:19'),(6,'hoge123','社員4','kndGSXk46zGIKxmIW3BGMTwAFvdbp-90C4Y5wQfGVSI',2,3,0,'2018-06-21 04:20:57','2018-06-22 08:00:55'),(7,'hoge456','社員5','P5R5uXPEOG77tvhAYlWir9-TmPFMejobXSUbhk540mM',3,4,0,'2018-06-22 04:57:08','2018-06-28 00:08:26'),(8,'hoge789','社員6','73_qwPZPQ6D6lgmV2hUXj8nuZnAuNjMBltYJwfGDMqo',2,4,0,'2018-06-22 05:08:04','2018-06-28 00:08:19'),(9,'testabc','社員7','5oHc_6SClk3bqvW8Wq-qPwVcxetcil04ms4MqGWcdm8',4,3,0,'2018-06-22 08:20:29','2018-06-26 09:00:12'),(10,'admin01','社員8','jYl-RUsDyjafYmnSMQjYq5Z83a4rk7VJNYtrh84hxqw',4,4,0,'2018-06-25 01:16:36','2018-06-28 00:08:05'),(11,'tttttttt','11111','JVijTU0glkyh0nKrJszOlRHYgFeVk81MngGrke0A8yU',2,4,0,'2018-06-28 01:10:02','2018-06-28 01:10:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-28 12:07:01
