<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="Content-Script-Type" content="text/javascript">
		<script type="text/javascript" src="./js/dialog.js"></script>
		<title>ユーザー管理画面</title>
		<link href="./css/managements.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }" />
					</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>


			<p id="titleName">ユーザー管理</p> <br />

			<a href="signup">新規登録</a>
<!-- 			<table border="1px">
			<tr><td>1</td><td>2</td></tr>
			<tr><td>1</td><td>2</td></tr>
			</table> -->
			<div class="users"><table border="1px">
				<c:forEach items="${users }" var="user">
					<div class="user">
						<div class="user-name">

						<tr><td><span class="id"> <c:out value="${user.id }" /></span></td>

						<td><span class="loginId"> <c:out value="${user.loginId }" /></span></td>
						<td><span class="name"> <c:out value="${user.name }" /></span></td>
						<td><span class="branchName"> <c:out value="${user.branchName }" /></span></td>
						<td><span class="departmentId"> <c:out value="${user.departmentName }" /></span></td>

						<td><form action="managements" method="post">

							<c:choose>
                				<c:when test="${user.isDeleted == 0 }">
                				<input type="hidden" name="id" value="${user.id}"/>
                				<input type="hidden" name="ChangeId" value="${user.isDeleted}"/>
                				<input type="submit" value="停止" onClick="return CheckDialog();"/><br />
                				</c:when>
                				<c:otherwise>
                					<input type="hidden" name="id" value="${user.id}"/>
                					<input type="hidden" name="ChangeId" value="${user.isDeleted}"/>
                					<input type="submit" value="復活" onClick="return CheckDialog();"/><br />
                				</c:otherwise>
                			</c:choose>
                		</form>

						<%-- <span class="isDeleted"> <c:out value="${user.isDeleted }" /></span>
						<a href="settings?id=${user.id}">停止・復活</a> <br />--%>
						<td><a href="settings?id=${user.id}">編集</a> </td></tr>

						</div>
					</div>
				</c:forEach></table>
			</div>
			<a href="./">戻る</a>
		</div>
	</body>
</html>