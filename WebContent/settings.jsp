<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${editUser.name}の設定</title>
		<link href="./css/login.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages }" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="settings" method="post"><br />
				<label for="loginId">ログインID(半角英数字6～20字以内)</label><br />
                <input name="loginId" value="${editUser.loginId}" id="loginId" /><br />

				<label for="name">名前(10文字以内)</label><br />
                <input name="name" value="${editUser.name}" id="name"/><br />

                <label for="password">パスワード(半角英数字6～20字以内)</label><br />
                <input name="password" type="password" id="password" value="${password }"/> <br />

                <label for="checkPassword">パスワード確認用(半角英数字6～20字以内)</label><br />
                <input name="checkPassword" type="password" id="checkPassword" value="${checkPassword}"/> <br />

                <%-- <label for="branchName">支店</label><br />
                <input name="branchName" value="${editUser.branchName}" id="branchName"/> <br />

                <label for="departmentName">部署・役職</label><br />
                <input name="departmentName" value="${editUser.departmentName}" id="departmentName"/> <br />--%>

				<c:if test="${loginUser.id != editUser.id }">
                <p>
                	<label for="branchName">支店</label><br />

					<select name="branchId">
                		<c:forEach items="${branches }" var="branch">
                			<c:if test="${branch.id == editUser.branchId }">
                				<option value="${branch.id }" selected>${branch.name }</option>
                			</c:if>
                			<c:if test="${branch.id != editUser.branchId }">
                				<option value="${branch.id }">${branch.name }</option>
                			</c:if>
                		</c:forEach>
                	</select>

				</p>

                <p>
                	<label for="departmentName">部署・役職</label><br />

					<select name="departmentId">
						<c:forEach items="${departments }" var="department">
                			<c:if test="${department.id == editUser.departmentId }">
                				<option value="${department.id }" selected>${department.name }</option>
                			</c:if>
                			<c:if test="${department.id != editUser.departmentId }">
                				<option value="${department.id }">${department.name }</option>
                			</c:if>
                			</c:forEach>
					</select>
				</p>
				</c:if>
				<c:if test="${loginUser.id == editUser.id }">
                	<input type="hidden" name="branchId" value="${editUser.branchId}" />
                	<input type="hidden" name="departmentId" value="${editUser.departmentId}" />
                </c:if>
                <input type="hidden" name="id" value="${editUser.id}" />
                <input type="submit" value="登録" /> <br />
                <a href="managements">戻る</a>
			</form>
		</div>
	</body>
</html>