<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./css/managements.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<p id="titleName">新規登録</p> <br />
			<div class="users">
			<form action="signup" method="post">
				<br /><label for="loginId">ログインID(半角英数字6～20字以内)</label><br /> <input name="loginId" id="loginId" value="${loginId }"/> <br />
				<label for="name">名前(10文字以内)</label><br /> <input name="name" id="name" value="${name }"/> <br />
				<label for="password">パスワード(半角英数字6～20字以内)</label><br /> <input name="password" type="password" id="password" value="${password}"/> <br />
				<label for="checkPassword">パスワード確認用</label><br /> <input name="checkPassword" type="password" id="checkPassword" value="${checkPassword }"/> <br />
				<%-- <label for="branchOfficeId">支店名</label><br /> <input name="branchOfficeId" id="branchOfficeId" /> <br />
				<label for="departmentId">部署または役職名</label><br /> <input name="departmentId" id="departmentId" /> <br />--%>
				<p>
                	<label for="branchName">支店</label><br />
					<select name="branchId">
						<option value="${1}">東京本社</option>
						<option value="${2}">新宿支店</option>
						<option value="${3}">横浜支店</option>
						<option value="${4}">名古屋支店</option>

					</select>
				</p>

                <p>
                	<label for="departmentName">部署・役職</label><br />
					<select name="departmentId">
						<option value="${1}">総務部</option>
						<option value="${2}">情報管理部</option>
						<option value="${3}">支店長</option>
						<option value="${4}">社員</option>

					</select>
				</p>
				<input type="submit" value="登録" /> <br />
			</form>
		</div></div>
		<a href="./managements">戻る</a>
	</body>
</html>