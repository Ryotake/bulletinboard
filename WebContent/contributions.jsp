<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/login.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="title">
			<h1 id="title">新規投稿</h1>
		</div>
		<div class="main-contents">

			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages }" var="message">
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="contributions" method="post"><br />
				<div>
					<label for="title">件名(30文字以内)</label><br />
					<input name="title" id="title" /><br />

					<label for="text">本文(1000文字以内)</label><br />
					<textarea name="text" cols="50" rows="20" id="text"/></textarea><br />

					<label for="category">カテゴリー(10文字以内)</label><br />
					<input name="category" id="category"><br />
				</div>
				<input type="submit" value="投稿">
				<a href="./">戻る</a>
			</form>
		</div>
	</body>
</html>