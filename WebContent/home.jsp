<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="Content-Script-Type" content="text/javascript">
		<script type="text/javascript" src="./js/DeleteDialog.js"></script>
		<title>ホーム画面</title>
		<link href="./css/home.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<c:if test="${not empty loginUser && loginUser.departmentId != 1}">
					<a href="contributions">新規投稿</a>
					<a href="./">ホーム</a>
					<a href="logout">ログアウト</a><br />
				</c:if>
				<c:if test="${not empty loginUser && loginUser.departmentId == 1}">
					<a href="contributions">新規投稿</a>
					<a href="managements">ユーザー管理</a>
					<a href="./">ホーム</a>
					<a href="logout">ログアウト</a><br />
					</c:if>
					<c:if test="${not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages }" var="message">
									<li><c:out value="${message }" />
									</c:forEach>
							</ul>
						</div>
						<c:remove var="errorMessages" scope="session"/>
					</c:if>

			</div>
		</div>


		<form action="./" method="get">
			<div class="serch-contributions">
			<table border="1px">
				<div class="serch">
					<c:if test="${not empty loginUser }">
						<tr><td><label for="serchCategory">カテゴリー検索</label><br />
                		<input name="serchCategory" id="serchCategory" value="${serchCategory}"/><br />

						<label for="serchStart">開始日</label><br />
                		<input type="date" name="serchStart" id="serchStart"/><br />

                		<label for="serchEnd">終了日</label><br />
                		<input type="date" name="serchEnd" id="serchEnd"/><br />

                		<input type="submit"value="検索" />
                		<c:remove var="serchCategory" scope="session"/></td></tr>

                		<%-- <input type="reset"value="クリア" id="reset"/> --%><br />

					</c:if>
				</div>
				</table>
			</div>
		</form>


		<div class="contributions">
			<c:forEach items="${messages }" var="message">
				<div class="contribution">
					<div class="user-name">

						<span class="title"> <c:out value="${message.title }" /></span><br />

						<c:forEach items="${ fn:split(message.text,'
						')}" var="text">
							${text}<%-- <span class="text"><c:out value="${text }" /></span> --%><br />
						</c:forEach>
						<label for="category">カテゴリー:</label>
						<span class="category"> <c:out value="${message.category }" /></span><br />
						<label for="name">投稿者:</label>
						<span class="name"> <c:out value="${message.name }" /></span><br />
						<label for="dateTime">日付:</label>
                		<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br />
                		<c:forEach items="${comment }" var="comment">
							<c:if test="${message.id == comment.contributionId }">
							<div class="comment">
								<c:forEach items="${ fn:split(comment.text,'
								')}" var="commentText">
									${commentText }<br />
                					<%-- <span class="comment"> <c:out value="${comment.text }" /></span> --%>
                				</c:forEach>
                				<span class="comment"> <c:out value="${comment.name }" /></span>
                				<span class="comment"> <c:out value="${comment.createdDate }" /></span>
                				<form action="delete" method="get">
                					<c:if test="${comment.userId == loginUser.id }">
                					<input type="hidden" name="id" value="${comment.id}"/>
                					<input type="submit" value="コメント削除" onClick="return DeleteDialog();"/><br />
                					</c:if>
                				</form>
                			</div>
                			</c:if>
                		</c:forEach>
                		<form action="delete" method="post"><br />
                			<c:if test="${loginUser.id == message.userId }">
                				<input type="hidden" name="id" value="${message.id}"/>
                				<input type="submit" value="投稿削除" onClick="return DeleteDialog();"/><br />
                			</c:if>
                		</form>
                		<form action="./" method="post"><br />
                			<input type="hidden" name="id" value="${message.id}"/>
                			<textarea name="comment" cols="50" rows="10" id="comment"></textarea><br />
                			<%-- <a href="home?id=${message.id }">コメント</a> --%>
                			<input type="submit" value="コメント"/>
                			<br />
                		</form>
                	</div>
                </div>
			</c:forEach>
		</div>
	</body>
</html>