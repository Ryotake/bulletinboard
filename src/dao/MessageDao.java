package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection,Message message){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions (");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(") VALUES ( ");
			sql.append(" ?"); //user_id
			sql.append(", ?"); //title
			sql.append(", ?"); //text
			sql.append(", ?"); //category
			sql.append(", CURRENT_TIMESTAMP");//created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<Message> getUserMessages(Connection connection, String startTime, String endTime, String category){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.name, ");
			sql.append("contributions.user_id, ");
			sql.append("contributions.id, ");
			sql.append("contributions.title, ");
			sql.append("contributions.text, ");
			sql.append("contributions.category, ");
			sql.append("contributions.created_date ");
			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");
			sql.append("WHERE contributions.created_date BETWEEN ");
			sql.append(" ? AND ? ");
			if(category != null && !category.isEmpty()){
				sql.append("AND contributions.category LIKE ? ");
			}

			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, startTime);
			if(endTime != null && !endTime.isEmpty()){
				String end = endTime.concat(" 23:59:59");
				ps.setString(2, end);

			}else{
				Date date= new Date();
		        long time = date.getTime();
				Timestamp ts = new Timestamp(time);
				ps.setTimestamp(2, ts);
			}

			if(category != null && !category.isEmpty()){
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<Message> ret = MessageList(rs);

			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Message> MessageList(ResultSet rs) throws SQLException{

		List<Message> ret = new ArrayList<Message>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Message messages = new Message();
				messages.setId(id);
				messages.setUserId(userId);
				messages.setName(name);
				messages.setTitle(title);
				messages.setText(text);
				messages.setCategory(category);
				messages.setCreatedDate(createdDate);

				ret.add(messages);
			}
			return ret;
		}finally{
			close(rs);
		}
	}


	public void isDeleteMessage(Connection connection, int contributionId){

		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM contributions WHERE id = ?";


			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, contributionId);

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
