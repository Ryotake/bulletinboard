package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public Comment getContribution(Connection connection, String contributionDate){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM contributions WHERE created_date = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, contributionDate);

			ResultSet rs = ps.executeQuery();
			int contributionId = rs.getInt("id");

			Comment comment = new Comment();
			comment.setContributionId(contributionId);

			return comment;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public void insert(Connection connection, Comment comment){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append("user_id");
			sql.append(", contribution_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES ( ");
			sql.append(" ?"); //user_id
			sql.append(", ?"); //contribution_id
			sql.append(", ?"); //text
			sql.append(", CURRENT_TIMESTAMP");//created_date
			sql.append(", CURRENT_TIMESTAMP");//updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getContributionId());
			ps.setString(3, comment.getText());
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<Comment> getUserComments(Connection connection){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.name, ");
			sql.append("comments.user_id, ");
			sql.append("comments.contribution_id, ");
			sql.append("comments.id, ");
			sql.append("comments.text, ");
			sql.append("comments.created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = CommentList(rs);

			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Comment> CommentList(ResultSet rs) throws SQLException{

		List<Comment> ret = new ArrayList<Comment>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int contributionId = rs.getInt("contribution_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setUserId(userId);
				comment.setContributionId(contributionId);
				comment.setName(name);
				comment.setText(text);
				comment.setCreatedDate(createdDate);

				ret.add(comment);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public void isDeleteComment(Connection connection, int commentId){

		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM comments WHERE id = ?";


			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, commentId);

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
