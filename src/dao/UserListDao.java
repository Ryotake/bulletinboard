package dao;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.SQLRuntimeException;

public class UserListDao {

	public List<User> getUsers(){

		Connection connection = null;
		PreparedStatement ps = null;
		try{
			connection = getConnection();
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("*  ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ON ");
			sql.append("users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ON ");
			sql.append("users.department_id = departments.id ");
			sql.append("ORDER BY users.id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException{

		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()){
				int id = Integer.parseInt(rs.getString("id"));
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branchId = Integer.parseInt(rs.getString("branch_id"));
				String branchName = rs.getString("branches.name");
				int departmentId = Integer.parseInt(rs.getString("department_id"));
				String departmentName = rs.getString("departments.name");
				int isDeleted = Integer.parseInt(rs.getString("is_deleted"));

				User users = new User();
				users.setId(id);
				users.setLoginId(loginId);
				users.setName(name);
				users.setBranchId(branchId);
				users.setBranchName(branchName);
				users.setDepartmentId(departmentId);
				users.setDepartmentName(departmentName);
				users.setIsDeleted(isDeleted);

				ret.add(users);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public User getUser(List<String> messages, int id){

		Connection connection = null;
		PreparedStatement ps = null;
		try{
			connection = getConnection();
			//String sql = "SELECT * FROM users WHERE id = ?";
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("*  ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ON ");
			sql.append("users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ON ");
			sql.append("users.department_id = departments.id ");
			sql.append("WHERE users.id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true){
				messages.add("不正なパラメータが入力されました");
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}


	public List<Branch> getBranches(){

		Connection connection = null;
		PreparedStatement ps = null;
		try{
			connection = getConnection();
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);

			if(branchList.isEmpty() == true){
				return null;
			}else{
				return branchList;
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException{

		List<Branch> ret = new ArrayList<Branch>();
		try{
			while(rs.next()){
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret.add(branch);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public List<Department> getDepartments(){

		Connection connection = null;
		PreparedStatement ps = null;
		try{
			connection = getConnection();
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Department> departmentList = toDepartmentList(rs);

			if(departmentList.isEmpty() == true){
				return null;
			}else{
				return departmentList;
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException{

		List<Department> ret = new ArrayList<Department>();
		try{
			while(rs.next()){
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");

				Department department = new Department();
				department.setId(id);
				department.setName(name);

				ret.add(department);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	/*private User toUser(ResultSet rs) throws SQLException{

		User editUser = new User();
		try{
			while(rs.next()){
				int id = Integer.parseInt(rs.getString("id"));
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branchId = Integer.parseInt(rs.getString("branch_id"));
				String branchName = rs.getString("branches.name");
				int departmentId = Integer.parseInt(rs.getString("department_id"));
				String departmentName = rs.getString("departments.name");
				int isDeleted = Integer.parseInt(rs.getString("is_deleted"));

				editUser.setId(id);
				editUser.setLoginId(loginId);
				editUser.setName(name);
				editUser.setBranchId(branchId);
				editUser.setBranchName(branchName);
				editUser.setDepartmentId(departmentId);
				editUser.setDepartmentName(departmentName);
				editUser.setIsDeleted(isDeleted);

			}
			return editUser;
		}finally{
			close(rs);
		}
	}*/

	public void isValidId(List<String> messages, String validId){

		Connection connection = null;
		PreparedStatement ps = null;
		int id = Integer.parseInt(validId);
		String checkId = null;
		try{
			connection = getConnection();
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				checkId = rs.getString("id");

			}

			if(StringUtils.isEmpty(checkId) == true){
				messages.add("不正なパラメーターが送信されました");
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public void getDepartmentId(List<String> messages, int id, int departmentId, int branchId){

		Connection connection = null;
		PreparedStatement ps = null;
		//List<User> users = new ArrayList<User>();
		int checkId = 0;
		int dCheckId = 0;
		int bCheckId = 0;
		try{
			connection = getConnection();
			String sql = "SELECT * FROM users WHERE department_id = ? AND branch_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, 3);
			ps.setInt(2, branchId);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				//User user = new User();
				id = rs.getInt("id");
				dCheckId = rs.getInt("department_id");
				bCheckId = rs.getInt("branch_id");

				//user.setId(id);
				//user.setDepartmentId(dCheckId);
				//user.setBranchId(bCheckId);

				//users.add(user);
				if(id != checkId && departmentId == dCheckId && branchId == bCheckId){
					messages.add("役職名が不正です");
				}

			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
