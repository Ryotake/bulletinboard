package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_deleted");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // loginId
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branchOfficeId
            sql.append(", ?"); // departmentId
            sql.append(", ?"); // isDeleted
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setString(4, String.valueOf(user.getBranchId()));
            ps.setString(5, String.valueOf(user.getDepartmentId()));
            ps.setString(6, String.valueOf(user.getIsDeleted()));
            ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public void update(Connection connection , User user){

		PreparedStatement ps = null;
		String password = user.getPassword();
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");

			if(password != null){
				sql.append(", password = ?");
			}

			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			if(password != null){
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());
				ps.setInt(6, user.getId());
			}else{
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getDepartmentId());
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginId, String password){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public void ChangeUser(Connection connection, int id, int isDeleted){

		PreparedStatement ps = null;
		try{
			String sql = "UPDATE users SET is_deleted = ? WHERE id = ?";
			if(isDeleted == 0){
				isDeleted = 1;
			}else{
				isDeleted = 0;
			}

			ps = connection.prepareStatement(sql);
			ps.setInt(1, isDeleted);
			ps.setInt(2, id);

			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}


	public List<User> toUserList(ResultSet rs) throws SQLException{

		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()){
				int userId = Integer.parseInt(rs.getString("id"));
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
	            String password = rs.getString("password");
	            int branchOfficeId = Integer.parseInt(rs.getString("branch_id"));
	            int departmentId = Integer.parseInt(rs.getString("department_id"));
	            int isDeleted = Integer.parseInt(rs.getString("is_deleted"));
	            Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");

	            User user = new User();
	            user.setId(userId);
	            user.setLoginId(loginId);
	            user.setName(name);
	            user.setPassword(password);
	            user.setBranchId(branchOfficeId);
	            user.setDepartmentId(departmentId);
	            user.setIsDeleted(isDeleted);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);

	            ret.add(user);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public void ValidUser(Connection connection, List<String> messages, String loginId, int id){

		PreparedStatement ps = null;
		String validLoginId = null;

		try{
			int userId = 0;
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				userId = rs.getInt("id");
				validLoginId = rs.getString("login_id");

			}
			if(loginId.equals(validLoginId) && userId != id){
				messages.add("このログインIDは既に使われています。");
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public void ValidUser(Connection connection, List<String> messages, String loginId){

		PreparedStatement ps = null;
		String validLoginId = null;

		try{
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				validLoginId = rs.getString("login_id");

			}
			if(loginId.equals(validLoginId)){
				messages.add("このログインIDは既に使われています。");
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
