package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter("/*")
public class LoginFilter implements Filter {


	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");

		String path = ((HttpServletRequest)request).getServletPath();

		if (!path.equals("/login") && !path.matches(".*.css")){
			if(user == null) {
				List<String> messages = new ArrayList<String>();
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("login");
				return;
			}
		}

		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
