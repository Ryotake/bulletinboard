package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(urlPatterns = {"/managements", "/settings", "/signup"})
public class AuthorityFilter implements Filter {


	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");

		HttpSession session = ((HttpServletRequest)request).getSession();

		if(user != null){
			int departmentId = user.getDepartmentId();
			if(departmentId != 1){
				List<String> messages = new ArrayList<String>();
				messages.add("アクセス権限がありません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("./");
				return;
			}
			chain.doFilter(request, response);
			return;
		}

		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
