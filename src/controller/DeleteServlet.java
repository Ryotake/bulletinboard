package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/delete"})
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int commentId = Integer.parseInt(request.getParameter("id"));
		new CommentService().getMessage(commentId);


		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int contributionId = Integer.parseInt(request.getParameter("id"));
		new MessageService().getMessage(contributionId);


		response.sendRedirect("./");

	}
}
