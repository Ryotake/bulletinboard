package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = {"/contributions"})
public class ContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("contributions.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		int loginUser = ((User)session.getAttribute("loginUser")).getId();

		List<String> messages = new ArrayList<String>();

		if(isValid(request, messages) == true){
			//String text = request.getParameter("text");

			Message message = new Message();
			message.setUserId(loginUser);
			message.setTitle(request.getParameter("title"));
			//message.setText(text.replaceAll("\n", "<br />"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));


			new MessageService().register(message);

			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("contributions.jsp").forward(request, response);
			//response.sendRedirect("contributions");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages){

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(title) == true){
			messages.add("件名を入力してください");
		}else if(title.length() > 30){
			messages.add("文字数が30文字を超過したため投稿できません。");
		}

		if(StringUtils.isEmpty(text) == true){
			messages.add("本文を入力してください");
		}else if(text.length() > 1000){
			messages.add("文字数が1000文字を超過したため投稿できません。");
		}

		if(StringUtils.isEmpty(category) == true){
			messages.add("カテゴリーを入力してください");
		}else if(category.length() > 10){
			messages.add("文字数が10文字を超過したため投稿できません。");
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
