package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/settings"})
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		String validId = request.getParameter("id");
		if(isValidId(messages, validId) == true){
			User editUser = new UserService().getUser(messages, Integer.parseInt(request.getParameter("id")));
			List<Branch> branches = new UserService().getBranches();
			List<Department> departments = new UserService().getDepartments();

			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("settings.jsp").forward(request, response);

		}else{

			session.setAttribute("errorMessages", messages);

		}

//		request.getRequestDispatcher("management.jsp").forward(request, response);
		response.sendRedirect("managements");

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if(isValid(request, messages) == true){
			new UserService().update(editUser);

		}else{
			String password = request.getParameter("password");
			String checkPassword = request.getParameter("checkPassword");

			session.setAttribute("errorMessages", messages);
			session.setAttribute("password", password);
			session.setAttribute("checkPassword", checkPassword);
			session.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("managements");
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException{

		String password = request.getParameter("password");
		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setName(request.getParameter("name"));

		if(password != null && !StringUtils.isEmpty(password)){
			editUser.setPassword(request.getParameter("password"));
		}

		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages){
		int id = Integer.parseInt(request.getParameter("id"));
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = null;
		String checkPassword = null;
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));


		if(request.getParameter("password") != null){
			password = request.getParameter("password");
		}

		if(request.getParameter("checkPassword") != null){
			checkPassword = request.getParameter("checkPassword");
		}

		if(!StringUtils.isEmpty(password) && !StringUtils.isEmpty(checkPassword)){
			if(!password.equals(checkPassword)){
				messages.add("パスワードが違います");
			}else if(password.length() < 6 || password.length() > 20){
				messages.add("パスワードは6～20文字以内で入力してください");
			}
		}

		if(StringUtils.isEmpty(loginId) == true){
			messages.add("ログインIDを入力してください");
		}else if(loginId.length() < 6 || loginId.length() > 20){
			messages.add("ログインIDは6～20文字以内で入力してください");
		}

		if(StringUtils.isEmpty(name) == true){
			messages.add("名前を入力してください");
		}else if(name.length() > 10){
			messages.add("名前は10文字以内で入力してください");
		}

		if(branchId != 1 && departmentId == 1){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId != 1 && departmentId == 2){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId == 1 && departmentId == 3){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId == 1 && departmentId == 4){
			messages.add("支店と部署の組み合わせが不正です");
		}

		new UserService().getDepartmentId(messages, id, departmentId, branchId);

		new UserService().getUser(messages, loginId, id);

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

	private boolean isValidId(List<String> messages, String validId){

//		if(StringUtils.isEmpty(validId) == true || validId == null){
//			messages.add("不正なパラメーターが送信されました");
//		}else if(validId.matches(".*[a-zA-Z].*")){
//			messages.add("不正なパラメーターが送信されました");
//		}
		if(validId.matches("^[0-9]+$")){
			new UserService().isValidId(messages, validId);
		}else{
			messages.add("不正なパラメーターが送信されました");
		}


		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
