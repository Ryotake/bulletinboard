package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserListDao;
import service.UserService;


@WebServlet(urlPatterns = {"/managements"})
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<User> users = new UserListDao().getUsers();
		request.setAttribute("users", users);
		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		int id = Integer.parseInt(request.getParameter("id"));
		int isDeleted = Integer.parseInt(request.getParameter("ChangeId"));

		if(isValid(request, messages) == true){
			new UserService().getUser(id, isDeleted);

		}else{
			session.setAttribute("errorMessages", messages);

		}
		response.sendRedirect("managements");

	}

	private boolean isValid(HttpServletRequest request, List<String> messages){

		int id = Integer.parseInt(request.getParameter("id"));
		User user = (User) request.getSession().getAttribute("loginUser");

		if(id == user.getId()){
			messages.add("自分自身を停止することはできません");
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

}
