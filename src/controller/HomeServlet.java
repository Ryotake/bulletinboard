package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Message;
import beans.User;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
		HttpSession session = request.getSession();

		boolean isShowMessageForm;
		if(user != null){
			isShowMessageForm = true;
		}else{
			isShowMessageForm = false;
		}

		String startTime = "2018-01-01 00:00:00";
		String endTime = null;
		String category = null;

		if(request.getParameter("serchCategory") != null && !request.getParameter("serchCategory").isEmpty()){

			category = request.getParameter("serchCategory");
			session.setAttribute("serchCategory", category);
		}

		if(request.getParameter("serchStart") != null && !request.getParameter("serchStart").isEmpty()){

			String start = request.getParameter("serchStart");
			startTime = start.concat(" 00:00:00");
		}

		if(request.getParameter("serchEnd") != null && !request.getParameter("serchEnd").isEmpty()){
			endTime = request.getParameter("serchEnd");
		}


		List<Message> messages = new MessageService().getMessage(startTime, endTime, category);
		List<Comment> comments = new CommentService().getComment();

		request.setAttribute("messages", messages);
		request.setAttribute("comment", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isValid(request, messages) == true){

			int contributionId = Integer.parseInt(request.getParameter("id"));
			Comment comment = new Comment();
			comment.setContributionId(contributionId);
			comment.setUserId(user.getId());
			comment.setText(request.getParameter("comment"));

			new CommentService().register(comment);

			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");
			//request.getRequestDispatcher("/home.jsp").forward(request, response);
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages){
		String comment = request.getParameter("comment");

		if(StringUtils.isEmpty(comment) == true){
			messages.add("コメントを入力してください。");
		}else if(comment.length() > 500){
			messages.add("コメントは500文字以内で入力してください");
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

}
