package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");

		if(isValid(request, messages) == true){

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
			user.setIsDeleted(0);

			new UserService().register(user);

			response.sendRedirect("managements");
		}else{
			session.setAttribute("loginId", loginId);
			session.setAttribute("name", name);
			session.setAttribute("password", password);
			session.setAttribute("checkPassword", checkPassword);
			session.setAttribute("errorMessages", messages);

			//response.sendRedirect("signup");
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages){
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		if(StringUtils.isEmpty(loginId) == true){
			messages.add("ログインIDを入力してください");
		}else if(loginId.length() < 6 || loginId.length() > 20){
			messages.add("ログインIDは6～20文字以内で入力してください");
		}

		if(StringUtils.isEmpty(password) == true){
			messages.add("パスワードを入力してください");
		}else if(password.length() < 6 || password.length() > 20){
			messages.add("パスワードは6～20文字以内で入力してください");
		}

		if(!password.equals(checkPassword)){
			messages.add("パスワードが違います");
		}

		if(StringUtils.isEmpty(name) == true){
			messages.add("名前を入力してください");
		}else if(name.length() > 10){
			messages.add("名前は10文字以内で入力してください");
		}


		if(branchId != 1 && departmentId == 1){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId != 1 && departmentId == 2){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId == 1 && departmentId == 3){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if(branchId == 1 && departmentId == 4){
			messages.add("支店と部署の組み合わせが不正です");
		}

		new UserService().getUser(messages, loginId);

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

}
