package beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private int userId;
	private String name;
	private String title;
	private String text;
	private String category;
	private Date createdDate;

	public void setId(int id){
		this.id = id;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public void setText(String text){
		this.text = text;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}

	public int getId(){
		return id;
	}

	public int getUserId(){
		return userId;
	}

	public String getName(){
		return name;
	}

	public String getTitle(){
		return title;
	}

	public String getText(){
		return text;
	}

	public String getCategory(){
		return category;
	}

	public Date getCreatedDate(){
		return createdDate;
	}

}
