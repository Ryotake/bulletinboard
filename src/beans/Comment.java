package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private int userId;
	private int contributionId;
	private String name;
	private String text;
	private Date createdDate;

	public void setId(int id){
		this.id = id;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public void setContributionId(int contributionId){
		this.contributionId = contributionId;
	}

	public void setText(String text){
		this.text = text;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}

	public int getId(){
		return id;
	}

	public int getUserId(){
		return userId;
	}

	public int getContributionId(){
		return contributionId;
	}

	public String getText(){
		return text;
	}

	public String getName(){
		return name;
	}

	public Date getCreatedDate(){
		return createdDate;
	}



}
