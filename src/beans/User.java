package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String name;
	private String password;
	private int branchOfficeId;
	private String branchOfficeName;
	private int departmentId;
	private String departmentName;
	private int isDeleted;
	private Date createdDate;
	private Date updatedDate;

	public void setId(int id){
		this.id = id;
	}

	public void setLoginId(String loginId){
		this.loginId = loginId;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public void setBranchId(int branchOfficeId){
		this.branchOfficeId = branchOfficeId;
	}

	public void setBranchName(String branchOfficeName){
		this.branchOfficeName = branchOfficeName;
	}

	public void setDepartmentId(int departmentId){
		this.departmentId = departmentId;
	}

	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}

	public void setIsDeleted(int isDeleted){
		this.isDeleted = isDeleted;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}

	public int getId(){
		return this.id;
	}

	public String getLoginId(){
		return this.loginId;
	}

	public String getName(){
		return this.name;
	}

	public String getPassword(){
		return this.password;
	}

	public int getBranchId(){
		return this.branchOfficeId;
	}

	public String getBranchName(){
		return this.branchOfficeName;
	}

	public int getDepartmentId(){
		return this.departmentId;
	}

	public String getDepartmentName(){
		return this.departmentName;
	}

	public int getIsDeleted(){
		return this.isDeleted;
	}

	public Date getCreatedDate(){
		return this.createdDate;
	}

	public Date getUpdatedDate(){
		return this.updatedDate;
	}

}
