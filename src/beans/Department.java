package beans;

import java.util.Date;

public class Department {

	private int id;
	private String name;
	private Date createdDate;
	private Date updatedDate;

	public void setId(int id){
		this.id = id;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}

	public int getId(){
		return this.id;
	}

	public String getName(){
		return this.name;
	}

	public Date getCreatedDate(){
		return this.createdDate;
	}

	public Date getUpdatedDate(){
		return this.updatedDate;
	}

}
