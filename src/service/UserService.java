package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Department;
import beans.User;
import dao.UserDao;
import dao.UserListDao;
import utils.CipherUtil;

public class UserService {

	public void register(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void update(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			String password = user.getPassword();
			if(password != null){
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void getUser(int id, int isDeleted){

		Connection connection = null;
		try{
			connection = getConnection();

			new UserDao().ChangeUser(connection, id, isDeleted);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void getUser(List<String> messages, String loginId, int id){

		Connection connection = null;
		try{
			connection = getConnection();

			new UserDao().ValidUser(connection, messages, loginId, id);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void getUser(List<String> messages, String loginId){

		Connection connection = null;
		try{
			connection = getConnection();

			new UserDao().ValidUser(connection, messages, loginId);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public User getUser(List<String> messages, int id){

		Connection connection = null;
		try{
			connection = getConnection();

			User editUser = new UserListDao().getUser(messages, id);

			commit(connection);

			return editUser;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public List<Branch> getBranches(){

		Connection connection = null;
		try{
			connection = getConnection();

			List<Branch> branches = new UserListDao().getBranches();

			commit(connection);

			return branches;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public List<Department> getDepartments(){

		Connection connection = null;
		try{
			connection = getConnection();

			List<Department> departments = new UserListDao().getDepartments();

			commit(connection);

			return departments;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public void getDepartmentId(List<String> messages, int id, int departmentId, int branchId){

		Connection connection = null;
		try{
			connection = getConnection();

			new UserListDao().getDepartmentId(messages, id, departmentId, branchId);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public void isValidId(List<String> messages, String validId){

		Connection connection = null;
		try{
			connection = getConnection();

			new UserListDao().isValidId(messages, validId);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

}
